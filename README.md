# FortiGate Config Parser

## Example Parsed Config

Config.conf:
```
#config-version=FGT60F-6.4.3-FW-build1778-201021:opmode=0:vdom=0:user=redskyadmin
#conf_file_ver=57290663263963449
#buildno=1778
#global_vdom=1
config system global
    set admintimeout 50
    set alias "FGT60FTK19001298"
    set gui-certificates enable
    set gui-fortisandbox-cloud enable
    set gui-ipv6 enable
    set gui-wireless-opensecurity enable
    set hostname "REDSKY_FG60F"
    set switch-controller enable
    set timezone 12
end
config system accprofile
    edit "prof_admin"
        set secfabgrp read-write
        set ftviewgrp read-write
        set authgrp read-write
        set sysgrp read-write
        set netgrp read-write
        set loggrp read-write
        set fwgrp read-write
        set vpngrp read-write
        set utmgrp read-write
        set wifi read-write
    next
    edit "address_rw"
        set fwgrp custom
        config fwgrp-permission
            set address read-write
        end
    next
    edit "super_admin_api"
        set secfabgrp read-write
        set ftviewgrp read-write
        set authgrp read-write
        set sysgrp read-write
        set netgrp read-write
        set loggrp read-write
        set fwgrp read-write
        set vpngrp read-write
        set utmgrp read-write
        set wifi read-write
    next
end
```

Example output.json:
```
{
  "metadata": {
    "config-version": "FGT60F-6.4.3-FW-build1778-201021",
    "opmode": "0",
    "vdom": "0",
    "user": "redskyadmin"
    "conf_file_ver": "57290663263963449",
    "buildno": "1778",
    "global_vdom": "1778"
  },
  "config": {
    "system": {
      "global": {
        "admintimeout": "50",
        "alias": "FGT60FTK19001298",
        ...
        "timezone": "12"
      },
      "accprofile": {
        "prof_admin": {
          "secfabgrp": "read-write",
          "ftviewgrp": "read-write",
          ...
          "wifi": "read-write"
        },
        "address_rw": {
          "fwgrp": "custom",
          "fwgrp-permission": {
            "address": "read-write"
          }
        },
        ...
      }
    }
  }
}
```

## Setup

```
git clone git@gitlab.com:python-projects8/fortigate-config-parser.git
cd fortigate-config-parser
# Create your virtual environment and activate it
pip install -r requirements.txt
```

## Usage

```
python fgparser.py -i config.conf <COMMAND>
```

## Available Commands

```
export - exports the file to the specified format
  required: 
    --format
      - JSON
      - CSV
      - XLSX (requires --outfile, --excel-config-file)
  optional:
    --outfile           -   the file to otput to
    --stdout            -   output to console
    --excel-config-file -   Excel configuration file, see [link](#Excel-Export-Options) 
```

## Excel Export Options

Excel export options are read from a JSON file. 

Example:
```
{
---Some Data to be Determine---
}
```