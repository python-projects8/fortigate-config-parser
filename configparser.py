import json
from loguru import logger
from sys import stdout
from argparse import ArgumentParser as AP
import os


LOG_FILE_LEVEL = 'DEBUG'

# Initialize Loguru logging
LEVELS = [
    "CRITICAL",
    "ERROR",
    "WARNING",
    "INFO",
    "DEBUG"
]
logger.remove()
logger.add(os.path.join("logs", "configparser", "configparser_{time:YYYY_MM_DD}.log"), level=LOG_FILE_LEVEL, rotation="00:00")
logger.add(os.path.join("logs", "configparser_error", "configparser_error_{time:YYYY_MM_DD}.log"), level="WARNING", rotation="00:00")

# Constants

# data to skip in the config (i.e. anything that 
# does not provide configuration to the system)
SKIP_DATA = [ 
    "\n",
    ""
]

# Merges two dictionaries together recursively
# Credit to (https://stackoverflow.com/questions/20656135/python-deep-merge-dictionary-data)
def _merge(source, destination):
    for key, value in source.items():
        if isinstance(value, dict):
            # get node or create one
            node = destination.setdefault(key, {})
            _merge(value, node)
        else:
            destination[key] = value

    return destination
# End credit to (https://stackoverflow.com/questions/20656135/python-deep-merge-dictionary-data)

# Builds metadata of config
def _get_metadata(data:dict) -> dict:
    # Initialize results
    results = {}

    # While true
    while True:
        # Try to pop off stack - If breaks, throw exception for invalid file 
        try:
            # All metadata lines start with '#'. If this line does
            # not start with '#', we have reached the end of the metadata
            if not data[0].startswith("#"):
                break
            
            # Pop line off the stack
            line = data[0]
            del data[0]

        except Exception as err:
            logger.critical("Reached end of file before expected. Please verify file is correct")
            exit(1)

        # If line in skip values, continue
        if line in SKIP_DATA:
            continue
        
        # Remove '#'
        line = line.replace("#", "")

        # Split by ':'
        key_value_pairs = line.split(":")

        # For each key/value pair
        for pair in key_value_pairs:
            # Check for invalid pair
            if "=" not in pair or len(pair.split("=")) > 2:
                logger.warning(f"Found invalid key/value pair in metadata: {pair}")
                continue
                
            # split by '='
            key, value = pair.split("=")

            # add key/value pair to results
            results[key] = value

    # Return results
    return results

# Get config stack
def _get_config_stack(filename:str) -> list:
    # open file
    with open(filename, "r") as f:

        # return array of lines split by \n
        return f.read().split("\n")

# Parse Config recursive function
def _parse_config_rec(data:list) -> tuple:
    line = None

    # Get the next non-skipped line 
    while True:
        # Try to pop line of the stack - break if fails
        try:
            if len(data) == 0:
                return ("EOF", None)

            # Pop line off the stack
            line = data[0]
            del data[0]
            line = line.strip()
        except Exception as err:
            logger.error(f"Error while grabbing next line: {err}")
            return (err, None)

        # If line is in list of skip data, continue
        if line in SKIP_DATA:
            continue

        # If line has reached the end of the full context -
        # relay this to parent stack frame
        if line == "end" or line == "next":
            return ("EOC", None)
        
        break

    # If line starts with config
    if line.startswith("config") or line.startswith("edit"):
        # Initialize Results
        results = {}

        # Get keys
        if line.startswith("config"):
            # Split keys [1::]
            keys = line.split(" ")[1::]

            # Remove quotes from keys
            for index, key in enumerate(keys):
                keys[index] = keys[index].replace("\"", "")

        elif line.startswith("edit"):
            # Split keys " ".join([1::])
            keys = [" ".join(line.split(" ")[1::]).replace("\"", "")]


        while True:
            # Since 'edit <vdom>' does not end with 'next', 
            # This check is required. This will look forward
            # to the next usable line and determine if it is
            # 'config vdom'. If so, it will return an 
            # 'End of Context' flag. 
            for _line in data:
                if _line.strip() in SKIP_DATA:
                    continue
                elif _line.strip() == "config vdom" and results != {}:
                    return (keys, results)
                else:
                    break
            
            # Recurse
            _res = _parse_config_rec(data)
            child_keys, value = _res

            # If end of context has been reached, break from loop
            if type(child_keys) is str and child_keys == "EOC":
                break

            # If child_keys is None (EOF has been reached), relay this
            # to the top of the call stack
            elif type(child_keys) is str and child_keys == "EOF":
                if len(results) > 0:
                    return (keys, results)
                else:
                    return ("EOF", None)
            # If an exception occured, continue
            elif type(child_keys) != str and type(child_keys) != list:
                continue

            # Create key in results for each child key
            ptr = results
            if len(child_keys) > 1:
                for child_key in child_keys[0:-1]:
                    if not ptr.get(child_key):
                        ptr[child_key] = {}
                    ptr = ptr[child_key]

            # Build results
            if ptr.get(child_keys[-1]) and type(value) is dict:
                ptr[child_keys[-1]] = _merge(value, ptr[child_keys[-1]])
            else:
                ptr[child_keys[-1]] = value

        # Return key/value pair
        return (keys, results)

    # If line start with set
    elif line.startswith("set") or line.startswith("unset"):
        # Split key/value pair
        line_split = line.split(" ")
        key = line_split[1]
        if line.startswith("set"):
            value = " ".join(line_split[2::])
        else:
            value = "--unset--"

        # In some cases, a value may continue onto
        # multiple lines, with the only indication being
        # the quotes (") were never matched - i.e.:
        #   " this is a message
        #     that continued to 
        #     multiple lines "
        #
        # Also, in order for JSON to be parsed, new lines 
        # cannot exist, there for the escaped newline character
        # (\\n) is added instead of a newline character (\n)
        if value.replace('\\"', "").count("\"") == 1:
            while value.replace('\\"', "").count("\"") < 2:
                value += f"\\n{data[0]}"
                del data[0]

        # Remove quotes from value
        value = value.replace("\"", "")
        
        # return key/value pair
        return ([key], value)

    
    print("should not be here")

# Parse config
@logger.catch
def parse_config(filename:str) -> dict:
    # Initialize results
    results = {}

    # Get config stack
    data = _get_config_stack(filename)

    # Get metatadata and add to results
    results["metadata"] = _get_metadata(data)

    while True:
        # Recurse
        child_keys, value = _parse_config_rec(data)

        # If end of context has been reached, continue
        if type(child_keys) is str and child_keys == "EOC":
            continue
        
        # If the end of the stack has been reached, break
        # from master loop and return results
        elif type(child_keys) is str and child_keys == "EOF":
            break
        
        # Create key in results for each child key
        ptr = results
        for child_key in child_keys[0:-1]:
            if not ptr.get(child_key):
                ptr[child_key] = {}
            ptr = ptr[child_key]

        # Build results
        if ptr.get(child_keys[-1]) and type(value) is dict:
            ptr[child_keys[-1]] = _merge(value, ptr[child_keys[-1]])
        else:
            ptr[child_keys[-1]] = value
            

    # return results
    return results

if __name__ == "__main__":
    # Argument parsing
    parser = AP()
    parser.add_argument("-d", "--debug", help=f"Debug level [0-5] (0=off, 1={LEVELS[0]}, 2={LEVELS[1]}, 3={LEVELS[2]}, 4={LEVELS[3]}, 5={LEVELS[4]})", required=False, default=0)
    parser.add_argument("-i", "--input-file", help="The file to parse", required=True)
    args = parser.parse_args()

    if args.debug != 0:
        logger.add(stdout, level=LEVELS[int(args.debug)-1])

    # Parse Config
    parsed_config = parse_config(args.input_file)

    # Print parsed config
    print(json.dumps(parsed_config))
